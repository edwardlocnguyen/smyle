# Generated by Django 4.0.6 on 2022-07-09 15:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('questions', '0002_alter_question_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='date',
        ),
        migrations.AddField(
            model_name='question',
            name='post_date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
