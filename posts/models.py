from django.db import models
from django.conf import settings

# Create your models here.

USER_MODEL = settings.AUTH_USER_MODEL

class Post(models.Model):
    post = models.TextField(null=True, blank=True)
    date = models.DateTimeField(auto_now=True, null=True, blank=True)
    # one author, many posts
    author = models.ForeignKey(
        USER_MODEL,
        related_name="posts",
        on_delete=models.CASCADE,
        null=True,
    )
    # one question, many posts
    question = models.ForeignKey(
        "questions.Question",
        related_name="posts",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return f"{self.author}'s Post on {self.date}"